package info.wesleyjefferson.encomendas.server;

import java.util.ArrayList;
import java.util.List;

import info.wesleyjefferson.encomendas.client.EncomendaService;
import info.wesleyjefferson.encomendas.shared.Encomenda;
import info.wesleyjefferson.encomendas.shared.TipoTransporte;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class EncomendaServiceImpl extends RemoteServiceServlet implements
		EncomendaService {

	private static final long serialVersionUID = 1L;
	private int counter;
	
	private List<Encomenda> repositorio;
	
	public EncomendaServiceImpl(){
		setRepositorio(new ArrayList<Encomenda>());
		counter = 0;
	}
	
	@Override
	public Encomenda insert(TipoTransporte tipoTransporte, String localOrigem,
			String localDestino, Double peso) {
		counter++;
		
		Encomenda enc = new Encomenda();
		enc.setTipoTransporte(tipoTransporte);
		enc.setLocalOrigem(localOrigem);
		enc.setLocalDestino(localDestino);
		enc.setPeso(peso);
		enc.setId(counter);
		
		getRepositorio().add(enc);
		
		return enc;
	}

	@Override
	public void delete(int id) {
		Encomenda enc = null;
		for (Encomenda encomenda : getRepositorio()) {
			if(encomenda.getId() == id){
				enc = encomenda;
				break;
			}
		}
		
		if(enc != null){
			getRepositorio().remove(enc);
		}
	}

	@Override
	public List<Encomenda> getAll() {
		return getRepositorio();
	}

	public List<Encomenda> getRepositorio() {
		return repositorio;
	}

	public void setRepositorio(List<Encomenda> repositorio) {
		this.repositorio = repositorio;
	}

}

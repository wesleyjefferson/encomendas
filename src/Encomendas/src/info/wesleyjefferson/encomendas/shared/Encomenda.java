package info.wesleyjefferson.encomendas.shared;

import java.io.Serializable;

public class Encomenda implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private TipoTransporte tipoTransporte;
	private String localOrigem;
	private String localDestino;
	private Double peso;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public TipoTransporte getTipoTransporte() {
		return tipoTransporte;
	}
	public void setTipoTransporte(TipoTransporte tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}
	public String getLocalOrigem() {
		return localOrigem;
	}
	public void setLocalOrigem(String localOrigem) {
		this.localOrigem = localOrigem;
	}
	public String getLocalDestino() {
		return localDestino;
	}
	public void setLocalDestino(String localDestino) {
		this.localDestino = localDestino;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(Double peso) {
		this.peso = peso;
	}
	
	public Double getPreco(){
		if(getTipoTransporte() == TipoTransporte.AEREO){
			return getPeso() * 40;
		}else if(getTipoTransporte() == TipoTransporte.MARITIMO){
			return getPeso() * 20;
		}
		
		return getPeso() * 30;
	}
	
	public Integer getPrazo(){
		if(getTipoTransporte() == TipoTransporte.AEREO){
			return 2;
		}else if(getTipoTransporte() == TipoTransporte.MARITIMO){
			return 20;
		}
		
		return 5;
	}
}

package info.wesleyjefferson.encomendas.shared;

public enum TipoTransporte {
	AEREO,
	MARITIMO,
	TERRESTRE
}

package info.wesleyjefferson.encomendas.shared;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;

public class Alerta {
	public static DialogBox exibeErro(String texto){
		return abreDialogo("Erro!", texto, "img/dialog_warning.png");
	}
	
	public static DialogBox exibeInfo(String texto){
		return abreDialogo("Informação", texto, "img/dialog_information.png");
	}
	
	private static DialogBox abreDialogo(String titulo, String texto, String pathIcone){
		final DialogBox dialog = new DialogBox(false);
		dialog.setText(titulo);
		
		Button btOk = new Button("OK", new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				dialog.hide();
			}
		});
		
		FlexTable contents = new FlexTable();
		contents.setWidget(0, 0, new Image(pathIcone));
		contents.setHTML(0, 1, texto);
		contents.setWidget(1, 0, btOk);
		
		contents.getFlexCellFormatter().setColSpan(1, 0, 2);
		contents.getFlexCellFormatter().setHorizontalAlignment(1, 0, HasHorizontalAlignment.ALIGN_CENTER);
		
		dialog.add(contents);
		
		dialog.center();
		
		return dialog;
	}
}

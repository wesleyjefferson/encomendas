package info.wesleyjefferson.encomendas.client;

import java.util.List;

import info.wesleyjefferson.encomendas.shared.Encomenda;
import info.wesleyjefferson.encomendas.shared.TipoTransporte;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface EncomendaServiceAsync {
	void insert(TipoTransporte tipoTransporte, String localOrigem,
			String localDestino, Double peso, AsyncCallback<Encomenda> callback);
	void delete(int id, AsyncCallback<Void> callback);
	void getAll(AsyncCallback<List<Encomenda>> callback);
}

package info.wesleyjefferson.encomendas.client;

import info.wesleyjefferson.encomendas.shared.Alerta;
import info.wesleyjefferson.encomendas.shared.Encomenda;
import info.wesleyjefferson.encomendas.shared.TipoTransporte;

import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class Encomendas implements EntryPoint {

	private Button btnInserir;
	private TextBox txtLocalOrigem;
	private TextBox txtLocalDestino;
	private ListBox cbTipoTransporte;
	private TextBox txtPeso;
	private Widget telaInsercao;
	private Widget telaConsulta;
	private Widget telaRemocao;
	private MenuBar menu;
	private EncomendaServiceAsync servico;

	public void onModuleLoad() {
		servico = GWT.create(EncomendaService.class);
		ServiceDefTarget target = (ServiceDefTarget) servico;
		String baseUrl = GWT.getModuleBaseURL();
		target.setServiceEntryPoint(baseUrl + "servico");
		
		telaInsercao = createTelaInsercao();
		telaConsulta = createTelaConsulta();
		telaRemocao = createTelaRemocao();
		menu = createMenu();
		
		RootPanel.get().add(menu);
		RootPanel.get().add(telaInsercao);
		RootPanel.get().add(telaConsulta);
		RootPanel.get().add(telaRemocao);
		
		telaInsercao.setVisible(true);
	}

	private MenuBar createMenu() {
		MenuBar menu = new MenuBar();
		menu.addItem("Inserir", new Command(){
			@Override
			public void execute() {
				hideAllScreens();
				telaInsercao.setVisible(true);
			}
		});
		menu.addItem("Consultar", new Command(){
			@Override
			public void execute() {
				hideAllScreens();
				carregarListagem(false);
				telaConsulta.setVisible(true);
			}
		});
		menu.addItem("Remover", new Command(){
			@Override
			public void execute() {
				hideAllScreens();
				carregarListagem(true);
				telaRemocao.setVisible(true);
			}
		});
		return menu;
	}

	private FlexTable createTelaInsercao() {
		btnInserir = new Button("Salvar");
		txtLocalOrigem = new TextBox();
		txtLocalDestino = new TextBox();
		cbTipoTransporte = new ListBox();
		txtPeso = new TextBox();
		
		cbTipoTransporte.addItem(TipoTransporte.AEREO.toString());
		cbTipoTransporte.addItem(TipoTransporte.MARITIMO.toString());
		cbTipoTransporte.addItem(TipoTransporte.TERRESTRE.toString());
		
		FlexTable layout = new FlexTable();
		layout.setText(0, 0, "Local de origem");
		layout.setWidget(0, 1, txtLocalOrigem);
		
		layout.setText(1, 0, "Local de destino");
		layout.setWidget(1, 1, txtLocalDestino);
		
		layout.setText(2, 0, "Tipo de frete");
		layout.setWidget(2, 1, cbTipoTransporte);
		
		layout.setText(3, 0, "Peso");
		layout.setWidget(3, 1, txtPeso);
		layout.setText(3, 2, "Kg");
		
		layout.setWidget(4, 0, btnInserir);
		
		layout.setVisible(false);
		
		btnInserir.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				if(validaCampos()){
					String tipoTransporteStr = cbTipoTransporte.getItemText(cbTipoTransporte.getSelectedIndex());
					TipoTransporte tipoTransporte = TipoTransporte.valueOf(tipoTransporteStr);
					Double peso = Double.parseDouble(txtPeso.getText());
					
					insereEncomenda(txtLocalOrigem.getText(), txtLocalDestino.getText(), tipoTransporte, peso);
					
					limparCampos();
				}
			}
		});
		
		return layout;
	}
	
	private Widget createTelaConsulta(){
		return new FlexTable();
	}
	
	private Widget createTelaRemocao(){
		return new FlexTable();
	}	
	
	private Boolean validaCampos(){
		
		txtLocalOrigem.removeStyleName("campoInvalido");
		txtLocalDestino.removeStyleName("campoInvalido");
		txtPeso.removeStyleName("campoInvalido");
		
		RegExp padrao = RegExp.compile("^[a-zÇ-Üá-ñ\\s]+$", "i");
		
		String origem = txtLocalOrigem.getText().trim();
		if(!padrao.test(origem) || origem.isEmpty()){
			txtLocalOrigem.addStyleName("campoInvalido");
			if(origem.isEmpty()){
				Alerta.exibeErro("O campo local de origem deve estar preenchido");
			}else{
				Alerta.exibeErro("O campo local de origem deve conter apenas letras");
			}
			
			return false;
		}
		
		String destino = txtLocalDestino.getText().trim();
		if(!padrao.test(destino) || destino.isEmpty()){
			txtLocalDestino.addStyleName("campoInvalido");
			if(destino.isEmpty()){
				Alerta.exibeErro("O campo local de destino deve estar preenchido");
			}else{
				Alerta.exibeErro("O campo local de destino deve conter apenas letras");
			}
			
			return false;
		}
		
		try{
			Double peso = Double.parseDouble(txtPeso.getText());
			if(peso < 0){
				Alerta.exibeErro("O campo Peso deve possuir um valor positivo.");
				txtPeso.addStyleName("campoInvalido");
				return false;
			}
			
		}catch(NumberFormatException e){
			Alerta.exibeErro("O campo Peso possui um valor inválido.");
			txtPeso.addStyleName("campoInvalido");
			return false;
		}
		
		
		return true;
	}
	
	private void carregarListagem(final Boolean remover){
		final FlexTable layout;
		if(remover){
			layout = (FlexTable)telaRemocao;
		}else{
			layout = (FlexTable)telaConsulta;
		}
		layout.removeAllRows();
		layout.setStyleName("flexTable");
		layout.setCellSpacing(0);
		
		final FlexCellFormatter formatter = layout.getFlexCellFormatter();
		
		servico.getAll(new AsyncCallback<List<Encomenda>>() {

			@Override
			public void onFailure(Throwable caught) {
				Alerta.exibeErro("Erro inesperado ao listar encomendas");
			}

			@Override
			public void onSuccess(List<Encomenda> result) {
				layout.setText(0, 0, "Origem");
				layout.setText(0, 1, "Destino");
				layout.setText(0, 2, "Transporte");
				layout.setText(0, 3, "Peso");
				layout.setText(0, 4, "Prazo");
				layout.setText(0, 5, "Preço");
				layout.getRowFormatter().addStyleName(0, "cabecalho");
				
				if(result.size() == 0){
					formatter.setColSpan(1, 0, 6);
					layout.setText(1,  0, "Não há encomendas cadastradas.");
				}else{
					for (int i = 0; i < result.size(); i++) {
						if(i % 2 == 0){
					       layout.getRowFormatter().addStyleName(i+1, "alterna");
						}
						
						final Encomenda enc = result.get(i);
						layout.setText(i+1, 0, enc.getLocalOrigem());
						layout.setText(i+1, 1, enc.getLocalDestino());
						layout.setText(i+1, 2, enc.getTipoTransporte().toString());
						layout.setText(i+1, 3, enc.getPeso().toString() + " kg");
						layout.setText(i+1, 4, enc.getPrazo().toString() + " dias");
						layout.setText(i+1, 5, "R$" + enc.getPreco().toString());
						
						if(remover){
							layout.setWidget(i+1, 6, new Button("Remover", new ClickHandler() {
								@Override
								public void onClick(ClickEvent event) {
									servico.delete(enc.getId(), new AsyncCallback<Void>() {
		
										@Override
										public void onFailure(Throwable caught) {
											Alerta.exibeErro("Erro inesperado ao remover a encomenda");
										}
		
										@Override
										public void onSuccess(Void result) {
											carregarListagem(true);
										}
									});
								}
							}));
						}
					}
				}
			}
		});		
	}	
	
	private void hideAllScreens(){
		telaInsercao.setVisible(false);
		telaConsulta.setVisible(false);
		telaRemocao.setVisible(false);
	}
	
	private void limparCampos(){
		txtPeso.removeStyleName("campoInvalido");
		txtLocalOrigem.removeStyleName("campoInvalido");
		txtLocalDestino.removeStyleName("campoInvalido");
		
		txtPeso.setText(null);
		txtLocalOrigem.setText(null);
		txtLocalDestino.setText(null);
		cbTipoTransporte.setItemSelected(0, true);
	}
	
	private void insereEncomenda(String localOrigem, String localDestino, TipoTransporte tipoTransporte, Double peso){
		servico.insert(tipoTransporte, localOrigem, localDestino, peso, new AsyncCallback<Encomenda>() {
			@Override
			public void onSuccess(Encomenda result) {
				Alerta.exibeInfo("Encomenda inserida com sucesso!<br/>Preço: R$"+ result.getPreco() + "<br/>Prazo: " + result.getPrazo() + " dias");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Alerta.exibeErro("Desculpe, aconteceu um erro inesperado.");
			}
		});
	}
	
	
	

}

package info.wesleyjefferson.encomendas.client;

import java.util.List;

import info.wesleyjefferson.encomendas.shared.Encomenda;
import info.wesleyjefferson.encomendas.shared.TipoTransporte;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("servico")
public interface EncomendaService extends RemoteService {
	Encomenda insert(TipoTransporte tipoTransporte, String localOrigem, String localDestino, Double peso);
	void delete(int id);
	List<Encomenda> getAll();
}
